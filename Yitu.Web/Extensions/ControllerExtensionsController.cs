﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace zqdn.Web.Extensions
{
    public static class ControllerExtensionsController
    {
        public static string ExtendErrors(this System.Web.Mvc.Controller controller)
        {
            IList<string> errors = new List<string>();
            foreach (var item in controller.ModelState.Values)
            {
                if (item.Errors.Count > 0)
                {
                    for (int i = item.Errors.Count - 1; i >= 0; i--)
                    {
                        errors.Add(item.Errors[i].ErrorMessage);
                    }
                }
            }
            return String.Join("<br />", errors);
        }
    }
}