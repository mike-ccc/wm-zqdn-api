﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Web.Helpers
{
 
        public enum Deparment
        {
            外科 = 1,
            内科 = 2,
            神经科 = 3
        }

        public enum DoctorOffice
        {
            主治医师 = 1,
            医师 = 2,
            助手 = 3
        }

        /// <summary>
        /// 注册默认医生角色为青年医生及医生助手
        /// 待院方（管理员）审核后可将其升级成主刀医生
        /// </summary>
        public enum Role
        {
            超级管理员 = 0,
            医院 = 1,
            医生 = 2,
            助手 = 3,
            患者 = 4,
            通用 = 5  ///此角色仅供”公共页面“使用，无其他作用！
            
        }

        /// <summary>
        /// 文章类型 
        /// </summary>
        public enum Article
        {
            名家手术视频 = 1,
            手术规范学习 = 2,
            名家谈手术 = 3
        }


        /// <summary>
        /// 预约流程 
        /// </summary>
        public enum Appoint
        {
            已预约 = 0,
            医生已处理 = 1,
            已付定金 = 2,
            已完成 = 3
        }
   
}
