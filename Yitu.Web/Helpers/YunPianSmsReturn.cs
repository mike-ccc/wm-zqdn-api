﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace zqdn.Web.Helpers
{
    public class YunPianSmsReturn
    {
        public int code { get; set; }
        public string msg { get; set; }
        public string detail { get; set; }
        public YunPianSmsResult result { get; set; }
    }
    public class YunPianSmsResult
    {
        /// <summary>
        /// 成功发送的短信个数
        /// </summary>
        public string count { get; set; }
        /// <summary>
        /// 扣费条数，70个字一条，超出70个字时按每67字一条计
        /// </summary>
        public string fee { get; set; }
        //短信id；群发时以该id+手机号尾号后8位作为短信id
        public string sid { get; set; }
    }
    public class YunPianSmsHelper
    {
        /**
		* 服务http地址
		*/
        private static string BASE_URI =ConfigurationSettings.AppSettings["YunPian_Uri"];
        /**
        * 服务版本号
        */
        private static string VERSION = ConfigurationSettings.AppSettings["YunPian_Version"]; 
        /**
        * 通用接口发短信的http地址
        */
        private static string URI_SEND_SMS = BASE_URI + "/" + VERSION +  ConfigurationSettings.AppSettings["YunPian_Uri_Send"]; 
        /**
        * 模板接口短信接口的http地址
        */
        private static string URI_TPL_SEND_SMS = BASE_URI + "/" + VERSION + ConfigurationSettings.AppSettings["YunPian_Uri_SendTpl"];

        private static string YunPian_ApiKey =  ConfigurationSettings.AppSettings["YunPian_Api_Key"];
        public static YunPianSmsReturn SendSms(string text, string mobile)
        {
            YunPianSmsReturn ret = new YunPianSmsReturn();
            JavaScriptSerializer js = new JavaScriptSerializer();
            //注意：参数必须进行Uri.EscapeDataString编码。以免&#%=等特殊符号无法正常提交
            string parameter = "apikey=" + YunPian_ApiKey + "&text=" + Uri.EscapeDataString(text) + "&mobile=" + mobile;
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI_SEND_SMS);
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(parameter);//这里编码设置为utf8
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
            {
                string objText = sr.ReadToEnd().Trim();
                ret = js.Deserialize<YunPianSmsReturn>(objText);
            }
            return ret;
        }

        public static YunPianSmsReturn TplSendSms(long tpl_id, string tpl_value, string mobile)
        {
            YunPianSmsReturn ret = new YunPianSmsReturn();
            JavaScriptSerializer js = new JavaScriptSerializer();
            string encodedTplValue = Uri.EscapeDataString(tpl_value);
            string parameter = "apikey=" + YunPian_ApiKey + "&tpl_id=" + tpl_id + "&tpl_value=" + encodedTplValue + "&mobile=" + mobile;
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI_TPL_SEND_SMS);
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(parameter);//这里编码设置为utf8
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
            {
                string objText = reader.ReadToEnd().Trim();
                ret = js.Deserialize<YunPianSmsReturn>(objText);
            }
            return ret;
        }
    }
}