﻿using System;

namespace zqdn.Web.Helpers
{
    public class Logs
    {
        /// <summary>
        /// 向本地，写入日志
        /// </summary>
        /// <param name="data"></param>
        public static void Execute(string data)
        {
            try
            {
                //得到日志目录
                var filepath = System.Web.HttpContext.Current.Server.MapPath("~/Logs/");

                if (!System.IO.Directory.Exists(filepath))
                {
                    System.IO.Directory.CreateDirectory(filepath);
                }

                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(filepath + DateTime.Now.ToString("yyyy-MM-dd") + ".log", true, System.Text.Encoding.UTF8))
                {
                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "：");
                    sw.WriteLine(data);
                    sw.WriteLine("---------------------------------------------");
                    sw.Close();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}