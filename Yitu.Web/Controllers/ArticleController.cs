﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Models.Article;
using zqdn.Services.Interfaces.CommonCenter;
using zqdn.Web.App_Start;
using zqdn.Web.ViewModels;
using zqdn.Web.Filters;

namespace zqdn.Web.Controllers
{

    public class ArticleController : Controller
    {
        private readonly IArticleService _iarticlerepository;

        public ArticleController(IArticleService iarticlerepository)
        {
            this._iarticlerepository = iarticlerepository;
        }
        // GET: CommonCenter
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List(int? tid)
        {
            try
            {
                var articlelist = _iarticlerepository.getAllArticle(tid ?? 0);
                var _result = Json(articlelist, JsonRequestBehavior.AllowGet);
                return _result;
            }
            catch (Exception ex) {
                return Json(new { Result = "error", msg = "服务异常" });
            }

        }

   
        // GET: 名家谈手术文章详情
        public JsonResult ArticleDetail(int aid)
        {
            try
            {
                var article = _iarticlerepository.getArticleByID(aid);
                var _result = Json(article, JsonRequestBehavior.AllowGet);
                //article.WatchCount = article.WatchCount + 1;
                //_iarticlerepository.updateArticle(article);
                return _result;
            }
            catch (Exception ex) {
                return Json(new { Result = "error", msg = "服务异常" });
            }

        }
        #region 
        public JsonResult addArticleStar(int aid, int type)
        {
            try
            {
                var article = _iarticlerepository.getArticleByID(aid);
                if (type == 1)
                {
                    article.StarCount = article.StarCount + 1;
                }
                else
                {
                    article.LikeCount = article.LikeCount + 1;
                }
                _iarticlerepository.updateArticleStar(article);
                return Json(new { Result = "success", msg = "操作成功" });

            }
            catch (Exception ex) {
                return Json(new { Result = "error", msg = "服务异常" });
            } 
        }

        [NormalAuthFilter(_role = (int)zqdn.Web.Helpers.Role.通用)]
        [HttpPost]
        public ActionResult Comments()
        {
            try
            {
                //if (SessionContext.CurrentUser!=null) 
                //{
            
                //}
                //else
                //{
                //    return Json(new { Result = "success", Url = Url.Action("Login", "User"), msg = "请先登录！" });
                //}
            }
            catch (Exception ex)
            {
               
              return Json(new { Result = "error", msg = ex.Message });
               
            }
            return View();
        }
        #endregion

        [HttpPost]
        public JsonResult updateStar(int id,int type)
        {
            try
            {
                var article = _iarticlerepository.getArticleByID(id);
                if (type == 1)
                {
                    article.WatchCount = article.WatchCount + 1;
                }
                else if(type == 2)
                {
                    article.LikeCount = article.LikeCount + 1;
                } 
                else {
                    article.StarCount = article.StarCount + 1;
                }
                _iarticlerepository.updateArticle(article);
                return Json(new { Result = "success", msg = "操作成功" });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "error", msg = "服务异常" });
            } 

        }
    }
}