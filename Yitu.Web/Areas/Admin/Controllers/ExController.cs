﻿using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Services.Interfaces.ProvinceCity;
using zqdn.Web.Helpers;
using zqdn.Web.ViewModels;
using zqdn.Web.Extensions;
using zqdn.Web.App_Start;
using zqdn.Web.Filters;
using zqdn.Services.Auth;
using zqdn.Models.Auth;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using zqdn.Services.Interfaces.Case;
using zqdn.Models.Ex;
using zqdn.Models.Case;
using zqdn.Services.Interfaces.Ex;

namespace zqdn.Web.Areas.Admin.Controllers
{

    public class ExController : Controller
    {
        private readonly IExService _exService;
        public ExController(IExService exService)
        {
            this._exService = exService;
        }

        // GET: Admin/User
        [NormalAuthFilter(_role = (int)zqdn.Web.Helpers.Role.超级管理员)]
        public ActionResult Index(int? page = 1)
        {
            var extpls = _exService.GetExList(page: (int)page, size: GlobalConfig.DefaultPageSize);
            var exViewModels = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<List<ExTpl>, List<ViewModels.ExViewModel>>().Map(extpls.ToList());
            var pagedList = new PagedList.StaticPagedList<ViewModels.ExViewModel>(exViewModels, extpls.GetMetaData());
            return View(pagedList);
        }

        public ActionResult Edit(int id){
           
            var extpl = _exService.getExByID(id);
            return View(extpl);
        }

        [HttpPost]
        public ActionResult Edit(ExTpl model)
        {
            string result = string.Empty;
            try
            {
                model.Option = "{" + model.Option + "}";
                model.ModifiedTime = DateTime.Now;
                _exService.Update(model);
                return Json(new { Result = "success", Msg = "编辑成功！" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", Msg = "提交失败！" });
            }
            
        }

        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(ExTpl model)
        {
            string result = string.Empty;
            try
            {
                model.Option = "{" + model.Option + "}";
                _exService.AddEx(model);
                return Json(new { Result = "success", Url = Url.Action("Index", "Ex"), Msg = "添加成功！" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "error", Msg = ex.ToString() });
            } 
        }
       
    }

}