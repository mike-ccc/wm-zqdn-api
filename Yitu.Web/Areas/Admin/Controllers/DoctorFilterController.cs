﻿using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Services.Interfaces.ProvinceCity;
using zqdn.Web.Helpers;
using zqdn.Web.ViewModels;
using zqdn.Web.Extensions;
using zqdn.Web.App_Start;
using zqdn.Web.Filters;
using zqdn.Services.Auth;
using zqdn.Models.Auth;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using zqdn.Services.Interfaces.Case;
using zqdn.Models.Ex;

namespace zqdn.Web.Areas.Admin.Controllers
{

    public class DoctorFilterController : Controller
    {
        private readonly IUserService _userService;
        public DoctorFilterController(IUserService userService)
        {
            this._userService = userService; 
        }

        // GET: Admin/User
        [NormalAuthFilter(_role = (int)zqdn.Web.Helpers.Role.超级管理员)]
        public ActionResult Index( string docMobile,int? page = 1)
        {
            var doctorFilters = _userService.GetDoctorFilters(page: (int)page, size: GlobalConfig.DefaultPageSize);
            var doctorFilterViewModels = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<List<DoctorFilter>, List<ViewModels.DoctorFilterViewModel>>().Map(doctorFilters.ToList());
            if (!string.IsNullOrEmpty(docMobile))
            {
                doctorFilterViewModels = doctorFilterViewModels.Where(o => o.Mobile.IndexOf(docMobile) >= 0).ToList();
            }
            var pagedList = new PagedList.StaticPagedList<ViewModels.DoctorFilterViewModel>(doctorFilterViewModels, doctorFilters.GetMetaData());
            ViewBag.docMobile = docMobile;
            return View(pagedList);
        }

        public ActionResult Edit(int id)
        {
            var extpl = _userService.getDoctorByID(id);
            return View(extpl);
        }

        [HttpPost]
        public ActionResult Edit(DoctorFilter model)
        {
            string result = string.Empty;
            try
            {
                model.ModifiedTime = DateTime.Now;
                _userService.UpdateDoctorFilter(model);
                return Json(new { Result = "success", Msg = "编辑成功！" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", msg = ex.ToString() });
            }
        }

        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(DoctorFilter model)
        {
            string result = string.Empty;
            try
            {
                DoctorFilter df = _userService.getDoctorByMobile(model.Mobile);
                if (df != null)
                {
                    return Json(new { Result = "error", Url = Url.Action("Edit", "DoctorFilter", new { id = df.ID }), Msg = "该手机号已存在！正在跳转至该记录" });
                }
                else
                {
                    _userService.AddDoctorFilter(model);
                    return Json(new { Result = "success", Url = Url.Action("Index", "DoctorFilter"), Msg = "添加成功！" });
                }
               
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", Msg = ex.ToString() });
            }
        }

        [HttpPost]
        public ActionResult delete(int id)
        {
            string result = string.Empty;
            try
            {
                DoctorFilter df = _userService.getDoctorByID(id);
                if (df != null) {
                    _userService.DeleteDoctorFilter(df);
                } 
                return Json(new { Result = "success", Url = Url.Action("Index", "DoctorFilter"), Msg = "删除成功！" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", Msg = ex.ToString() });
            }
        }
         
    }
}