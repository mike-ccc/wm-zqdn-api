﻿using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Services.Interfaces.ProvinceCity;
using zqdn.Web.Helpers;
using zqdn.Web.ViewModels;
using zqdn.Web.Extensions;
using zqdn.Web.App_Start;
using zqdn.Web.Filters;
using zqdn.Services.Auth;
using zqdn.Models.Auth;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using zqdn.Services.Interfaces.Case;
using zqdn.Models.Ex;

namespace zqdn.Web.Areas.Admin.Controllers
{

    public class GoodsController : Controller
    {
        private readonly IGoodService _goodServie;
        public GoodsController(IGoodService goodServie)
        {
            this._goodServie = goodServie;
        }

        // GET: Admin/User
        [NormalAuthFilter(_role = (int)zqdn.Web.Helpers.Role.超级管理员)]
        public ActionResult Index(int? page = 1)
        {
            var goods = _goodServie.GetGoodsList(page: (int)page, size: GlobalConfig.DefaultPageSize);
            var goodsViewModels = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<List<Goods>, List<ViewModels.GoodsViewModel>>().Map(goods.ToList());
            var pagedList = new PagedList.StaticPagedList<ViewModels.GoodsViewModel>(goodsViewModels, goods.GetMetaData());
            return View(pagedList);
        }

        public ActionResult Edit(int id)
        {
            var extpl = _goodServie.getGoodsByID(id);
            return View(extpl);
        }

        [HttpPost]
        public ActionResult Edit(Goods model)
        {
            string result = string.Empty;
            try
            { 
                model.ModifiedTime = DateTime.Now;
                _goodServie.Update(model);
                return Json(new { Result = "success", Msg = "编辑成功！" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", msg = ex.ToString() });
            } 
        }

        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(Goods model)
        {
            string result = string.Empty;
            try
            {  
                _goodServie.AddGood(model); 
                return Json(new { Result = "success", Url = Url.Action("Index", "Goods"), Msg = "添加成功！" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "error", Msg = ex.ToString()});
            }
             
        }
    }
}