﻿using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Services.Interfaces.ProvinceCity;
using zqdn.Web.Helpers;
using zqdn.Web.ViewModels;
using zqdn.Web.Extensions;
using zqdn.Web.App_Start;
using zqdn.Web.Filters;
using zqdn.Services.Auth;
using zqdn.Models.Auth;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using zqdn.Services.Interfaces.Case;
using zqdn.Models.Ex;

namespace zqdn.Web.Areas.Admin.Controllers
{

    public class IntegerController : Controller
    {
        private readonly IChangeLogService _changelogServie;
        private readonly IGoodService _goodService;
        public IntegerController(IChangeLogService changelogService,IGoodService goodService)
        {
            this._changelogServie = changelogService;
            this._goodService = goodService;
        }

        // GET: Admin/User
        [NormalAuthFilter(_role = (int)zqdn.Web.Helpers.Role.超级管理员)]
        public ActionResult Index(int? page = 1)
        {
            var changelogs = _changelogServie.getAllChangeLog(page: (int)page, size: GlobalConfig.DefaultPageSize);
            var changelogsViewModels = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<List<ChangeLog>, List<ViewModels.IntegerViewModel>>().Map(changelogs.ToList());
            var pagedList = new PagedList.StaticPagedList<ViewModels.IntegerViewModel>(changelogsViewModels, changelogs.GetMetaData());

            List<Goods> goodList = _goodService.getAllGood();
            ViewBag.goodList = goodList;
            return View(pagedList);
        }

        public void ExportExcel()
        {
            String str = WebConfigurationManager.ConnectionStrings["DB_Zqdn"].ToString();
            SqlConnection conn = new SqlConnection();
            string sql = @"select a.Name 用户姓名,b.Name 兑换人姓名,c.Name 奖品,c.RequireJF 所需积分,b.Address 地址,b.CreatedTime 兑换时间 from Users as a
                    join ChangeLogs as b
                    on a.ID = b.UID
                    join Goods as c
                    on b.GID = c.ID";
            conn.ConnectionString = str;
            if (conn.State != System.Data.ConnectionState.Open)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            adapter.Fill(table);
            CreateExcel(table, "application/ms-excel", "兑换记录明细");
        }

        public void CreateExcel(DataTable dt, string FileType, string FileName)
        {
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.Buffer = true;
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
            Response.AppendHeader("Content-Disposition", "attachment;filename=\"" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls\"");
            Response.ContentType = FileType;
            string colHeaders = string.Empty;
            string ls_item = string.Empty;
            DataRow[] myRow = dt.Select();
            int i = 0;
            int cl = dt.Columns.Count;

            for (int key = 0; key < cl; key++)
            {
                Response.Output.Write(dt.Columns[key] + "\t");
            }
            Response.Output.Write("\n");

            foreach (DataRow row in myRow)
            {
                for (i = 0; i < cl; i++)
                {
                    if (i == (cl - 1))
                    {
                        ls_item += row[i].ToString().Replace("\r", "").Replace("\n", "") + "\n";
                    }
                    else
                    {
                        ls_item += row[i].ToString().Replace("\r","").Replace("\n","") + "\t";
                    }
                }
                Response.Output.Write(ls_item);
                ls_item = string.Empty;
            }
            Response.Output.Flush();
            Response.End();
        }

    }
}