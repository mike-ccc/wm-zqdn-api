﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using zqdn.Services.Interfaces.CommonCenter;
using zqdn.Web.App_Start;
using zqdn.Web.Controllers;
using zqdn.Web.Helpers;
using zqdn.Web.ViewModels;
using zqdn.Web.Extensions;

namespace zqdn.Web.Areas.Admin.Controllers
{
    public class ArticleController : Controller
    {
        private readonly IArticleService _iarticlerepository;
        public ArticleController(IArticleService iarticlerepository)
        {
       
            this._iarticlerepository = iarticlerepository;
  
        }
        // GET: Admin/Article
        public ActionResult Index(int? page = 1)
        {
            var articlepages = _iarticlerepository.getAllArticlePage(page: (int)page, size: GlobalConfig.DefaultPageSize);
            return View(articlepages);
        }

        public ActionResult Create()
        {
            ViewBag.DoctorID = 1;// new ApiController().SetOptionItem(1),null);
            return View(new zqdn.Web.ViewModels.ArticleViewModel());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ArticleViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //var user = null;//_userService.GetUserByID(vm.DoctorID);
                    zqdn.Models.Article.Article article = new zqdn.Models.Article.Article
                    {
                        Title = vm.Title,
                        Content = vm.Content,
                        CreateUserID = SessionContext.CurrentUser.ID,
                        CreatedTime = DateTime.Now,
                        ModifiedTime = DateTime.Now,
                        Type = vm.Type,
                        VideoUrl = vm.VideoUrl,
                        Pdfurl = vm.Pdfurl,
                        DoctorID = vm.DoctorID,
                        Desc = vm.Desc,
                        //Auth = user.Name,
                        IsDeleted = false
                    };
                    _iarticlerepository.addArticle(article);

                    return Json(new { Result = "success", Url = Url.Action("Index", "Article"), Msg = "添加手术资料成功！" });
                }
                else
                {
                    throw new Exception(this.ExtendErrors());
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    statusCode = 500,
                    Msg = ex.Message
                });
            }
        }

        public ActionResult Edit(int aid)
        {
            var article = _iarticlerepository.getArticleByID(aid);
            var articleViewModel = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<zqdn.Models.Article.Article, zqdn.Web.ViewModels.ArticleViewModel>().Map(article);
            //ViewBag.DoctorID = new ApiController().SetOptionItem(_userService.GetUserByRoleID((int)zqdn.Web.Helpers.Role.医生), article.DoctorID);
            return View(articleViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ArticleViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   // var user = _userService.GetUserByID(vm.DoctorID);
                    var article = _iarticlerepository.getArticleByID(vm.ID);
                    EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<zqdn.Web.ViewModels.ArticleViewModel, zqdn.Models.Article.Article>().Map(vm,article);
                   // article.Auth = user.Name;
                    article.VideoUrl = vm.VideoUrl;
                    article.Pdfurl = vm.Pdfurl;
                    _iarticlerepository.updateArticle(article);

                    return Json(new { Result = "success", Url = Url.Action("Edit", "Article", new { aid=vm.ID }), Msg = "修改手术资料成功！" });
                }
                else
                {
                    throw new Exception(this.ExtendErrors());
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    statusCode = 500,
                    Msg = ex.Message
                });
            }
        }
    }
}