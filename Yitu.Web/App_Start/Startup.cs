﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using zqdn.Repositories;
using zqdn.Repositories.Interfaces;

namespace zqdn.Web
{
    public class Startup
    {
        public static void Run()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>()
                .WithParameter("nameOrConnectionString", "DB_Zqdn")
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(DatabaseFactory).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(Services.ServiceContext).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            // logging
            builder.Register(c => (Services.Interfaces.ILoggingService)NLog.LogManager.GetLogger("*", typeof(Services.LoggingService)))
                .As<Services.Interfaces.ILoggingService>();

            builder.RegisterFilterProvider();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        
        }
    }
}