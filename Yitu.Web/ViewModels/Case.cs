﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class CaseTplViewModel
    {
        public DateTime WriteTime { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string ZyNo { get; set; }
        public string CNo { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string DepNo { get; set; }

        public string Weight { get; set; }
        public string Med_YDQ { get; set; }
        public string Med_WCQ { get; set; }
        public string Med_SXQ { get; set; }

        public string MedCom_YDQ { get; set; }
        public string MedCom_WCQ { get; set; }
        public string MedCom_SXQ { get; set; }
        public string CaseImg { get; set; }
        

        public string ActiveHos { get; set; }
        public string ActiveID { get; set; }
        public string OpenID { get; set; }
        public int Status { get; set; }
        public int ID { get; set; }
    }

    public class CaseAfterTplViewModel
    {
        public DateTime WriteTime { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string ZyNo { get; set; }
        public string CNo { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string DepNo { get; set; }

        public string Weight { get; set; }

        public string MedCf_BTFN { get; set; }
        public string MedCf_YMTMD { get; set; }

        public string Med_PW_1 { get; set; }
        public string Med_PW_2 { get; set; }
        public string Med_PW_3 { get; set; }
        public string Med_PW_4 { get; set; }

        public string Med_GZ { get; set; }

        public string OpenID { get; set; }

        public string CaseImg { get; set; }

    }

    public class ApplyTplViewModel
    {
        public string MetCity { get; set; }
        public string SpeakCity { get; set; }
        public string SpeakHos { get; set; }
        public string SpeakDep { get; set; }
        public string SpeakPost { get; set; }
        public string SpeakPhone { get; set; }
        public string SpeakContent { get; set; }
        public string SpeakTimePlan { get; set; }
        public string JoinCount { get; set; }
        public string SpeakRate { get; set; }
    }

    public class ChangeViewModel
    {
        public int GID { get; set; }
        public int Socure { get; set; }
        public string Address { get; set; }
        public string RequireJF { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }

        public string OpenID { get; set; }
    }

}
