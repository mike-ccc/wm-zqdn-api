﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class ExViewModel
    {
        public int ID { get; set; }
        public string openID { get; set; }
        public int EID { get; set; }
        public int UID { get; set; }
        public string Answer { get; set; }
        public int Type { get; set; }
        public bool isTrue { get; set; }
        public string Title { get; set; }
        public string Option { get; set; }
        public string TureOption { get; set; }
    }
}