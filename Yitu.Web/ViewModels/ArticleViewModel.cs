﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class ArticleViewModel
    {

        public int ID { get; set; }
        [Required(ErrorMessage = "请填写手术标题！")]
        [DisplayName("手术标题")]
        public string Title { get; set; }
        [Required(ErrorMessage = "请填写资料类型！")]
        [DisplayName("资料类型")]
        public int Type { get; set; }
        [Required(ErrorMessage = "请填写资料内容！")]
        [DisplayName("资料内容")]
        public string Content { get; set; }
        [Required(ErrorMessage = "请填写手术描述！")]
        [DisplayName("手术描述")]
        public string Desc { get; set; }

        [Required(ErrorMessage = "请填写手术医生！")]
        [DisplayName("手术医生")]
        public int DoctorID { get; set; }
        [DisplayName("手术视频")]
        public string VideoUrl { get; set; }
        [DisplayName("手术规范PPT")]
        public string Pdfurl { get; set; }
    }
}