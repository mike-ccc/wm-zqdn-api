﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class UseriewModel
    {
        public string UserName { get; set; }
        public string Password { get;set;}
        public string Name { get; set; }
        public string VerifyCode { get; set; }
        public string Mobile { get; set; }
        public string Area { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Hospital { get; set; }
        public string Deparment { get; set; }
        public string OpenId { get; set; }
        public string NikeName { get; set; }
        public string HeadImg { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string Integral { get; set; }
    }

    public class UserieScourewModel
    {
        public int Integral { get; set; }
        public string openID { get; set; }
    }
}