﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class DoctorFilterViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Hospital { get; set; }
        public string Mobile { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }
    }

}