﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class GoodsViewModel
    {

        public int ID { get; set; }


        public string Name { get; set; }

        public string RequireJF { get; set; }

        public string GoodImg { get; set; }

        public int Stock { get; set; }

        public bool IsDeleted { get; set; }
    }

}