﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class ApplicationUser
    {
  

        /// <summary>
        /// 以系统用户编号作为账户的编号
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [MaxLength(64)]
        public string Name { get; set; }

        public string Username { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        public string  Province{ get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 医院
        /// </summary>
        public string Hospital { get; set; }

        public int Integral { get; set; }

        public string NikeName { get; set; }

      
        public string OpenId { get; set; }



        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(256)]
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        [MaxLength(256)]
        public string Password { get; set; }

        public int Role { get; set; }
    }
}