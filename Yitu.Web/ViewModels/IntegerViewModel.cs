﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zqdn.Web.ViewModels
{
    public class IntegerViewModel
    {
        public string GoodName { get; set; }
 
        public DateTime CreateDate { get; set; }
        public string UserName { get; set; }
 
        public int ID { get; set; }

        public int GID { get; set; }

        public int UID { get; set; }

        public int Socure { get; set; }

        public string Address { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }
        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }
    }

}