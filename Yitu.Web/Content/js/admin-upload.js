﻿$(function () {

            $(function () {
                var uploader = new plupload.Uploader({
                    browse_button: 'upload-video', //触发文件选择对话框的按钮，为那个元素id
                    url: '/api/UploadApprove/',
                    multi_selection: false,
                    max_file_size: '100mb',
                    filters: [
                    { title: "Video files", extensions: "mp4,flv,ogg" }
                    ],
                });
                //绑定各种事件，并在事件监听函数中做你想做的事
                uploader.bind('FilesAdded', function (uploader, files) {
                    uploader.start(); //调用实例对象的start()方法开始上传文件，当然你也可以在其他地方调用该方法
                });
                uploader.bind('UploadProgress', function (uploader, file) {
                    //console.log(uploader);
                    $("#upload-video").html("文件上传中，请稍等..." + file.percent + "%");
                    if (file.percent == 100) {
                        $("#upload-video").html("上传完成！点击可重新上传");

                    }

                });


                uploader.bind('FileUploaded', function (up, file, result) {

                    $(".upload-video").val(result.response);
                    $("#VideoUrl").val(result.response);

                });

                uploader.init();

                var uploader1 = new plupload.Uploader({
                    browse_button: 'upload-ppt', //触发文件选择对话框的按钮，为那个元素id
                    url: '/api/UploadApprove/',
                    multi_selection: true,
                    max_file_size: '1mb',
                    filters: [
                    { title: "Image files", extensions: "jpg,jpeg,png" }
                    ],
                });
                //绑定各种事件，并在事件监听函数中做你想做的事
                uploader1.bind('FilesAdded', function (uploader, files) {
                    uploader.start(); //调用实例对象的start()方法开始上传文件，当然你也可以在其他地方调用该方法
                });
                uploader1.bind('UploadProgress', function (uploader, file) {
                    $("#upload-ppt").html("文件上传中，请稍等..." + file.percent + "%");
                    if (file.percent == 100) {
                        $("#upload-ppt").html("上传完成！点击可重新上传");

                    }

                });
                var _completeurl = "";
                uploader1.bind('FileUploaded', function (up, file, result) {
                    _completeurl = _completeurl + "," + result.response;
                    $(".upload-ppt").val(_completeurl);
                    $("#Pdfurl").val(_completeurl);
                });

                uploader1.init();
            });

});