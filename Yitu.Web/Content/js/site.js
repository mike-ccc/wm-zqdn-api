﻿///form表单验证、提交
var formAjax = !function ($) {
    $.fn.BindFormAjax = function () {
        var form = $(".form");
        var acion = $(form).attr("action");
        //var err_msg = "<i ref='refid' class='msg-trip'>err</i>";
        jQuery(".form").validate({
            //当点击提交按钮时执行submitHandler函数
            submitHandler: function (form) {
                jQuery(form).ajaxSubmit({
                    url: acion,
                    data: jQuery(form).formSerialize(),
                    type: 'POST',
                    beforeSubmit: function () {
                        $(".formsubmit").dialog('处理中，请稍等...');
                    },
                    success: function (data) {
                        var _resllt = data.Result;
                        var _returnurl = data.Url;
                        var _msg = data.msg;
                        if (_resllt == "success") {
                            $(".formsubmit").dialog(_msg);
                            setTimeout('location.href ="' + _returnurl + '"', 1500);///成功后跳转
                        }
                        else {
                            $(".formsubmit").dialog(_msg);
                            if (_returnurl.indexOf("login") >= 0) {
                                setTimeout('location.href ="' + _returnurl + '"', 1500);///未登录时跳转
                            }
                        }
                    }
                });
                //当验证没通过时不予以提交表单
                return false;
            }
        });

        ///get请求方式 拦截错误信息
        $(".link_get").on("click", function (data) {
            var _url = $(this).attr("get-url");

            $.ajax({
                type: 'GET',
                url: _url,
                success: function (data) {
                    var _resllt = data.Result;
                    var _returnurl = data.returnurl;
                    var _msg = data.msg;

                    if (_resllt == "error") {

                        $(".link_get").dialog(_msg);
                    }
                    else {
                        location.href = _url;
                    }

                },
                error: function (error) {
                    $(".link_get").dialog(error.msg);
                }
            });
        });
    }

    $.fn.GetCity = function (pid) {
        $.post("/api/GetCity/", { pid: pid }, function (data) {
            var option = "<option value=''>请选择...</option>";
            $("#city").html(option);
            var obj = eval("(" + data + ")");
            for (var item in obj) {
                option += "<option value=" + obj[item].ID + ">" + obj[item].Name + "</option>";
            }
            $("#city").html(option);
        });
    }

    $.fn.GetHospital = function (cid) {
        $.post("/api/GetHospital/", { cid: cid }, function (data) {
            var option = "<option value=''>请选择...</option>";
            $("#hospital").html(option);
            var obj = eval("(" + data + ")");
            for (var item in obj) {
                option += "<option value=" + obj[item].ID + ">" + obj[item].Name + "</option>";
            }
            $("#hospital").html(option);
        });
    }

    $.fn.GetAssist = function (hid) {
        $.post("/api/GetAssist/", {roleid : 3, hid: hid }, function (data) {
            var radiohtml = "";
            $("#assistlist").html(radiohtml);
            var obj = eval("(" + data + ")");
            for (var item in obj) {
                radiohtml += "<p class='items'><input type='radio' class = 'required' name='assistname' value=" + obj[item].ID + ">" + obj[item].Name + "</p>";
            }
            $("#assistlist").html(radiohtml);
        });
    }

}(jQuery);


///弹出层封装///
!function ($) {
    $.fn.dialog = function (mes) {
        var defaults = {
            boxTitle: "提示信息"
        };
        var options = $.extend(defaults);
        layer.open({
            content: mes
        })
    }
}(jQuery);



$(function (data) {
    $("button[type='submit']").on("click", function () {
        $(this).BindFormAjax();
    });

    $("#province").on("change", function () {
        $(this).GetCity($("#province").val());
    });

    $("#city").on("change", function () {
        $(this).GetHospital($("#city").val());
    });

    $("#hospital").on("change", function () {
        $(this).GetAssist($("#hospital").val());
    });
});