﻿!function ($) {
$.fn.BindAjaxPost = function (opts) {

    var defaults = {
        form: $(this).parents("form:first"),
        action: $(this).parents("form:first").attr("action"),
        box: $("#MessageBox"),
        boxTitle: "提示信息",
        dialogType: "modal",
        content:"",
    };
    var options = $.extend(defaults, opts);
    var form = options.form;
    $(this).click(function () {
        var btn = $(this);
        $(btn).button('loading');
        var data = $(form).serialize();
        $.post(options.action, $(form).serialize(), function (re) {
            if (re.Result) {
                if (options.dialogType != "modal") {
                    $(options.box).html(re.Msg || options.content).dialog({
                        modal: true,
                        title: options.boxTitle,
                        position: { my: "center", at: "center", of: $(form) },
                        show: {
                            effect: "blind",
                            duration: 500
                        },
                        resizable: false
                    });
                }
                else {
                    $(options.box).find(".modal-title").html(options.boxTitle);
                    $(options.box).find(".modal-body").html(re.Msg || options.content);
                    $(options.box).modal('show');
                }
                if (!!re.Url) {
                    if (re.Url == 'none') {
                        if ($(btn).hasClass("hascountdown")) {
                            $(btn).BindCountDown({});
                        }
                        return;
                    }
                    else
                        setTimeout(function () { top.location = re.Url; }, 1000);
                }
                else
                    setTimeout(function () { top.location = self.location; }, 1000);
            }
            else {
                if (options.dialogType != "modal") {
                    $(options.box).html(re.Msg || options.content).dialog({
                        modal: true,
                        title: options.boxTitle,
                        position: { my: "center", at: "center", of: $(form) },
                        buttons: [
                            {
                                text: '确定', click: function () {
                                    $(this).dialog("close");
                                }
                            }
                        ],
                        show: {
                            effect: "blind",
                            duration: 500
                        },
                        resizable: false
                    });
                }
                else {
                    $(options.box).find(".modal-title").html(options.boxTitle);
                    $(options.box).find(".modal-body").html(re.Msg || options.content);
                    $(options.box).modal('show');
                }
                if (!!re.Url) {
                    setTimeout(function () { top.location = re.Url; }, 3000);
                }
            }
            $(btn).button("reset");
        }).error(function () {
            $(btn).button("reset");
        });
        return false;
    });
}
}(jQuery);


/**
 * 页面提交绑定
*/
!function ($) {
    $("#btnSave").BindAjaxPost({ box: "#ModalMessageBox" });
    $("#btnLogin").BindAjaxPost({ box: "#ModalMessageBox" });
    $(".unfinished").BindAjaxPost({ box: "#ModalMessageBox", content: "模块开发中..." });
}(window.jQuery);
