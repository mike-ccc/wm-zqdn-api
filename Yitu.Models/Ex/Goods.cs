﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Ex
{
    public class Goods : ModelBase
    {
        /// <summary>
        /// Case ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 奖品名
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 所需积分
        /// </summary>
        [MaxLength(20)]
        public string RequireJF { get; set; }


        /// <summary>
        /// 图片
        /// </summary>
        [MaxLength(200)]
        public string GoodImg { get; set; }


         /// <summary>
        /// 库存
        /// </summary>
        public int Stock { get; set; }
        

    
        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
