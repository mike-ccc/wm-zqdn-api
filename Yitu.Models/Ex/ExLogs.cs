﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Ex
{
    public class ExLogs : ModelBase
    {
        /// <summary>
        /// ExLOg ID
        /// 1: 阅读学术资讯 2：最强比拼答题 3：上传病例审核通过 4：搜索病例
        /// </summary>
        [Key]
        public int ID { get; set; }

        public int UID { get; set; }

        public int EID { get; set; }

        public int Type { get; set; }

        public string Answer { get; set; }

        public bool isTrue { get; set; }
        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
