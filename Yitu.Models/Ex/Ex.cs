﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Ex
{
    public class ExTpl : ModelBase
    {
        /// <summary>
        /// Case ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [MaxLength(200)]
        public string Title { get; set; }

        /// <summary>
        /// 选项
        /// </summary>
        [MaxLength(500)]
        public string Option { get; set; }

        /// <summary>
        /// 正确答案
        /// </summary>
        [MaxLength(200)]
        public string TureOption { get; set; }

     


        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
