﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.ProvinceCity
{
   public class CityDetails
    {
        [Key]
        public int ID { get; set; }

        public int? Sort { get; set; }
        public int? CreateUserID { get; set; }
        public int? LastUpdateUserID { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsDeleted { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        public int ParentID { get; set; }
        public int Depth { get; set; }
    }
}
