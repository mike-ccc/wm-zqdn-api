﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.ProvinceCity
{
    public class Hospital
    {
        [Key]
        public int ID { get; set; }
        public int CityID { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
