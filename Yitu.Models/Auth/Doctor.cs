﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace zqdn.Models.Auth
{
   public class DoctorFilter : ModelBase {
        /// <summary>
        /// Doctor ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }
        public string Hospital { get; set; }
        public string Mobile { get; set; }

        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
