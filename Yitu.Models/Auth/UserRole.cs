﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Auth
{
   public class UserRole : ModelBase
    {
       /// <summary>
       /// 编号
       /// </summary>
       [Key]
       public int ID { get; set; }

       /// <summary>
       /// 角色编号
       /// </summary>
       public int RoleID { get; set; }

       /// <summary>
       ///角色名称
       /// </summary>
       public string Name { get; set; }

       /// <summary>
       /// 角色描述
       /// </summary>
       public string Desc { get; set; }

    }
}
