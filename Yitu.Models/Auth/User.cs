﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Auth
{
   public class User : ModelBase
    {
       /// <summary>
       /// 用户ID
       /// </summary>
       [Key]
       public int ID { get; set; }

       /// <summary>
       /// 用户名
       /// </summary>
       [Required, MaxLength(32)]
       public string UserName { get; set; }

       /// <summary>
       /// 登录密码
       /// </summary>
       [Required, MaxLength(32)]
       public string PassWord { get; set; }

       /// <summary>
       /// 姓名
       /// </summary>
       [Required, MaxLength(32)]
       public string Name { get; set; }


       /// <summary>
       ///区域 
       /// </summary>
       [Required]
       public string Area { get; set; }
       
       /// <summary>
       ///省份 
       /// </summary>
       [Required]
       public string Province { get; set; }

       /// <summary>
       ///城市 
       /// </summary>
       public string City { get; set; }

       /// <summary>
       ///医院 
       /// </summary>
       [Required]
       public string Hospital { get; set; }

       /// <summary>
       ///科室 
       /// </summary>
       public string Deparment { get; set; }

       /// <summary>
       ///医生职称 
       /// </summary>
       public string DoctorOffice { get; set; }

       /// <summary>
       ///手机 
       /// </summary>
       public string Mobile { get; set; }

       /// <summary>
       ///医师资格证书 
       /// </summary>
       public string DoctorCertificate { get; set; }

       /// <summary>
       ///角色ID 
       /// </summary>
       public int RoleID { get; set; }

       /// <summary>
       /// 领域特长
       /// </summary>
       public string Domain { get; set; }

       /// <summary>
       ///账号状态
       /// </summary>
       public int Status { get; set; }

       /// <summary>
       

       /// <summary>
       ///nikename
       /// </summary>
       public string NikeName { get; set; }

       /// <summary>
       ///OPENID
       /// </summary>
       public string OpenId { get; set; }

       /// <summary>
       ///headimg
       /// </summary>
       public string HeadImg { get; set; }

       /// <summary>
       ///积分
       /// </summary>
       public int Integral { get; set; }

       /// <summary>
       ///是否删除
       /// </summary>
       public bool IsDeleted { get; set; }


    }
}
