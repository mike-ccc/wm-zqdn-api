﻿using System;

namespace zqdn.Models
{
    public interface ITimestamp
    {
        DateTime CreatedTime { get; set; }
        DateTime ModifiedTime { get; set; }
    }
}
