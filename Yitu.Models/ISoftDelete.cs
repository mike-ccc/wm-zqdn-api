﻿using System;

namespace zqdn.Models
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
        DateTime? DeletedTime { get; set; }
    }
}
