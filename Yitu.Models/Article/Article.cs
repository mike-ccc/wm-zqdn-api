﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Article
{
   public class Article : ModelBase
    {
        /// <summary>
        /// 文章ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 文章标题
        /// </summary>
        [MaxLength(50)]
        [DisplayName("标题")]
        public string Title { get; set; }

        /// <summary>
        /// 文章类型
        /// </summary>
        [Required]
        [DisplayName("类型")]
        public int Type { get; set; }

        /// <summary>
        /// 文章简介
        /// </summary>
        [MaxLength(200)]
        [DisplayName("描述")]
        public string Desc { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Required]
        [DisplayName("内容")]
        public string Content { get; set; }


        /// <summary>
        /// 视频地址
        /// </summary>
        [MaxLength(50)]
        [DisplayName("手术视频")]
        public string VideoUrl { get; set; }

        /// <summary>
        /// pdf地址
        /// </summary>
        [DisplayName("手术规范PPT")]
        public string Pdfurl { get; set; }

        /// <summary>
        /// 缩略图地址
        /// </summary>
        [MaxLength(50)]
        public string ThumbImg { get; set; }
       
        /// <summary>
        /// 作者
        /// </summary>
        [MaxLength(10)]
        [DisplayName("手术医生")]
        public string Auth { get; set; }

        /// <summary>
        /// 浏览次数
        /// </summary>
        
        public int WatchCount { get; set; }


        /// <summary>
        /// 点赞次数
        /// </summary>

        public int StarCount { get; set; }


        /// <summary>
        /// 收藏次数
        /// </summary>

        public int LikeCount { get; set; }
       

        /// <summary>
        /// 创建者
        /// </summary>
        public int CreateUserID { get; set; }

        /// <summary>
        /// 医生ID
        /// </summary>
        public int DoctorID { get; set; }


        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
