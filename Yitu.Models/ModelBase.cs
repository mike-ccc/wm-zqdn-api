﻿using System;

namespace zqdn.Models
{
    public class ModelBase : ITimestamp
    {
        public ModelBase()
        {
            this.CreatedTime = DateTime.Now;
            this.ModifiedTime = DateTime.Now;
        }

        #region implement for ITimestamp
        public DateTime CreatedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
        #endregion
    }
}
