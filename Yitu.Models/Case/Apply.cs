﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Apply
{
    public class Apply : ModelBase
    {
        /// <summary>
        /// Case ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 开会城市
        /// </summary>
        [MaxLength(20)]
        public string MetCity { get; set; }

        /// <summary>
        /// 讲者信息
        /// </summary>
        [MaxLength(20)]
        public string SpeakCity { get; set; }

        [MaxLength(50)]
        public string SpeakHos { get; set; }

        [MaxLength(50)]
        public string SpeakDep { get; set; }

        [MaxLength(50)]
        public string SpeakPost { get; set; }

        [MaxLength(50)]
        public string SpeakPhone { get; set; }

        [MaxLength(500)]
        public string SpeakContent { get; set; }

        [MaxLength(500)]
        public string SpeakTimePlan { get; set; }

        [MaxLength(50)]
        public string JoinCount { get; set; }

        [MaxLength(50)]
        public string SpeakRate { get; set; }

        /// <summary>
        ///创建人
        /// </summary>
        public int CreateUserID { get; set; }

        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
