﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Models.Case
{
    public class CaseTpl : ModelBase
    {
        /// <summary>
        /// Case ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// 填写时间
        /// </summary>
        public DateTime WriteTime { get; set; }

        /// <summary>
        /// 病例类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 住院号
        /// </summary>
        [MaxLength(50)]
        public string ZyNo { get; set; }

        /// <summary>
        /// 床号
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CNo { get; set; }

        /// <summary>
        /// 科室
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string DepNo { get; set; }


        /// <summary>
        /// 姓名
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [MaxLength(10)]
        public string Sex { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        [MaxLength(10)]
        public string Age { get; set; }
       
        /// <summary>
        /// 体重
        /// </summary>
        [MaxLength(10)]
        public string Weight { get; set; }

       
        /// <summary>
        /// 用药方案
        /// </summary>
        [MaxLength(200)]
        public string Med_YDQ { get; set; }
        [MaxLength(200)]
        public string Med_WCQ { get; set; }
        [MaxLength(200)]
        public string Med_SXQ { get; set; }


        /// <summary>
        /// 合并用药
        /// </summary>
        [MaxLength(200)]
        public string MedCom_YDQ { get; set; }
        [MaxLength(200)]
        public string MedCom_WCQ { get; set; }
        [MaxLength(200)]
        public string MedCom_SXQ { get; set; }



        /// <summary>
        /// 病例截图
        /// </summary>
        [MaxLength(4000)]
        public string CaseImg { get; set; }


        /// <summary>
        /// 完成医院
        /// </summary>
        [MaxLength(50)]
        public string ActiveHos { get; set; }

        /// <summary>
        /// 完成人ID
        /// </summary>
        [MaxLength(50)]
        public string ActiveID { get; set; }



        /// <summary>
        /// 术后镇痛病例征集
        /// </summary>
        /// 基础字段共用

        /// <summary>
        /// 药物配方
        /// </summary>
        [MaxLength(200)]
        public string MedCf_BTFN { get; set; }
        [MaxLength(200)]
        public string MedCf_YMTMD { get; set; }

        /// <summary>
        /// 配伍药物
        /// </summary>
        [MaxLength(200)]
        public string Med_PW_1 { get; set; }
        [MaxLength(200)]
        public string Med_PW_2 { get; set; }
        [MaxLength(200)]
        public string Med_PW_3 { get; set; }
        [MaxLength(200)]
        public string Med_PW_4 { get; set; }

        /// <summary>
        /// 归转情况
        /// </summary>
        [MaxLength(500)]
        public string Med_GZ { get; set; }


        /// <summary>
        ///创建人
        /// </summary>
        public int CreateUserID { get; set; }

        public int Status { get; set; }


        /// <summary>
        ///是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
