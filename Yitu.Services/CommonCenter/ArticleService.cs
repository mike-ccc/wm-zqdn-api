﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Article;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.CommonCenter;
using zqdn.Services.Interfaces.CommonCenter;
using zqdn.Repositories;

namespace zqdn.Services.CommonCenter
{
    public class ArticleService : BaseService<Article>, IArticleService
    {
        private readonly IArticleRepository _articlerepository;
        private readonly IUnitOfWork _unitOfWork;

        public ArticleService(IArticleRepository articlerepository, IUnitOfWork unitOfWork)
            : base(articlerepository, unitOfWork)
        {
            this._articlerepository = articlerepository;
            this._unitOfWork = unitOfWork;
        }

        public List<Article> getAllArticle(int? tid)
        {  
           return _articlerepository.GetAll().Where(o =>o.Type == tid && o.IsDeleted == false).ToList();
        }

        public IPagedList<Article> getAllArticlePage(int page, int size)
        {
            var query = _articlerepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }

        public Article getArticleByID(int aid)
        {
            return _articlerepository.GetById(aid);
        }

        public void addArticle(Article obj)
        {
            _articlerepository.Add(obj);
            _unitOfWork.Commit();
        }
        

        public void updateArticle(Article obj)
        {
            _articlerepository.Update(obj);
            _unitOfWork.Commit();
        }

        public void updateArticleStar(Article obj)
        {
            _articlerepository.Update(obj);
            _unitOfWork.Commit();
        }

        
    }
}
