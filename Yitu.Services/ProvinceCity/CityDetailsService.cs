﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.ProvinceCity;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.ProvinceCity;

namespace zqdn.Services.ProvinceCity
{
   public class CityDetailsService : BaseService<CityDetails>, Interfaces.ProvinceCity.ICityDetailsService
    {
        private ICityDetailsRepository cityDetailsRepository;
        private IHospitalRepository hospitalRepository;
        private IUnitOfWork unitOfWork;
        public CityDetailsService(ICityDetailsRepository cityDetailsRepository, IHospitalRepository hospitalRepository
            , IUnitOfWork unitOfWork)
            : base(cityDetailsRepository, unitOfWork)
        {
            this.cityDetailsRepository = cityDetailsRepository;
            this.hospitalRepository = hospitalRepository;
            this.unitOfWork = unitOfWork;
        }



        public IList<CityDetails> GetByParentID(int pid)
        {
            return cityDetailsRepository.GetMany(o => o.ParentID == pid).ToList();
        }

        public IList<CityDetails> GetByDepth(int depth)
        {
            return cityDetailsRepository.GetMany(o => o.Depth == depth).ToList();
        }

        public CityDetails GetByID(int ID)
        {
            return cityDetailsRepository.GetById(ID);
        }

        public Hospital GetHospitalByID(int ID)
        {
            return hospitalRepository.GetById(ID);
        }

        public List<Hospital> GetHospitalByCID(int CID)
        {
            return hospitalRepository.GetAll().Where(o => o.CityID == CID).ToList();
        }
    }
}
