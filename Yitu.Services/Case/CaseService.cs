﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.ase;
using zqdn.Services.Interfaces.Case;
using zqdn.Repositories;
using zqdn.Models.Case;
using zqdn.Models.Apply;

namespace zqdn.Services.Case
{
    public class CaseService : BaseService<CaseTpl>, ICaseService
    {
        private readonly ICaseRepository _caserepository;
        private readonly IApplyRepository _applyrepository;
        
        private readonly IUnitOfWork _unitOfWork;

        public CaseService(ICaseRepository caserepository, IApplyRepository applyrepository, IUnitOfWork unitOfWork)
            : base(caserepository, unitOfWork)
        {
            this._caserepository = caserepository;
            this._applyrepository = applyrepository;
            this._unitOfWork = unitOfWork;
        }

        public List<CaseTpl> getAllCase(string title)
        {
            return _caserepository.GetAll().Where(o => (string.IsNullOrEmpty(title) || o.Name.IndexOf(title) >= 0) && o.IsDeleted == false && o.Status == 1).ToList();
        }

        public CaseTpl getCaseByID(int id)
        {
            return _caserepository.GetById(id);
        }


        public void AddCase(CaseTpl casetpl)
        {
            _caserepository.Add(casetpl);
            _unitOfWork.Commit();
        }

        public void AddApply(Apply applytpl)
        {
            _applyrepository.Add(applytpl);
            _unitOfWork.Commit();
        }


        public void UpdateCase(CaseTpl casetpl) {
            _caserepository.Update(casetpl);
            _unitOfWork.Commit();
        }
        public CaseTpl getCaseByZyNo(string zyno)
        {
            return _caserepository.GetAll().Where(o => o.ZyNo == zyno).FirstOrDefault();
        }
        public IPagedList<CaseTpl> GetCaseList(int page, int size)
        {
            var query = _caserepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }
    }
}
