﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Ex;
using zqdn.Services.Interfaces;
using zqdn.Utils;

namespace zqdn.Services.Interfaces.Case
{
    public interface IGoodService : IBaseService<Goods>
    {


        /// <summary>
        /// 奖品列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        List<Goods> getAllGood();

        void AddGood(Goods goodtpl);
        Goods getGoodsByID(int id);
        IPagedList<Goods> GetGoodsList(int page, int size);
    }
}
