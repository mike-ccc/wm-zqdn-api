﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Ex;
using zqdn.Services.Interfaces;
using zqdn.Utils;

namespace zqdn.Services.Interfaces.Ex
{
    public interface IExService : IBaseService<ExTpl>
    {

        List<ExTpl> getAllEx();

        ExTpl getExByID(int id);

        void AddEx(ExTpl extpl);

        void AddExLog(ExLogs exlogtpl);

        void AddExChange(ChangeLog log);

        ExLogs getLogByType(int tid,int uid);
        List<ExLogs> getLogsByType(int tid, int uid);

        ExLogs getLogByEid(int eid,int uid);

        List<ChangeLog> GetChangeLogsByUID(int uid);

        IPagedList<ExTpl> GetExList(int page, int size);
    }
}
