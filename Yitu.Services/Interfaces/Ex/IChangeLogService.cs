﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Ex;
using zqdn.Services.Interfaces;
using zqdn.Utils;

namespace zqdn.Services.Interfaces.Case
{
    public interface IChangeLogService : IBaseService<ChangeLog>
    {


        /// <summary>
        /// 奖品列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
   
        IPagedList<ChangeLog> getAllChangeLog(int page, int size);
    }
}
