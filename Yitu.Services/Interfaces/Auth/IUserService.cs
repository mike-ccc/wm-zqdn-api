﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Auth;
using zqdn.Services.Interfaces;
using zqdn.Utils;

namespace zqdn.Services.Auth
{
    public interface IUserService : IBaseService<User>
    {
        /// <summary>
        /// 登陆验证
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User UserLogin(string userName, string password,string openid,int type);

        /// <summary>
        /// 根据ID查询用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User GetUserByID(int id);

        User GetUserByOprnID(string openid);

        DoctorFilter getDoctorByMobile(string mobile);
        DoctorFilter getDoctorByID(int id);

        /// <summary>
        /// 根据手机号查询用户
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        User GetUserByMobile(string mobile);

        /// <summary>
        /// 查询术者列表
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        List<User> GetUserByHid();

        /// <summary>
        /// 根据角色查询
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        List<User> GetUserByRoleID(int roleid);


        /// <summary>
        /// 查询用户角色
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<UserRole> GetUserRoles();

        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="obj"></param>
        void AddUser(User user);

        void AddDoctorFilter(DoctorFilter doctorfilter);

        void DeleteDoctorFilter(DoctorFilter doctorfilter);

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="obj"></param>
        void UpdateUser(User user);

        void UpdateDoctorFilter(DoctorFilter doctorfilter);

         //<summary>
         //管理员获得所有用户
         //</summary>
         //<returns></returns>
        IPagedList<User> GetUsers(int page, int size);

        /// <summary>
        /// 获取白名单列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        IPagedList<DoctorFilter> GetDoctorFilters(int page, int size);
    }
}
