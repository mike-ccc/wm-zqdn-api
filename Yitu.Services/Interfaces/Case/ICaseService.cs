﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Case;
using zqdn.Models.Apply;
using zqdn.Services.Interfaces;
using zqdn.Utils;

namespace zqdn.Services.Interfaces.Case
{
    public interface ICaseService : IBaseService<CaseTpl>
    {


        /// <summary>
        /// 获取病例列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        List<CaseTpl> getAllCase(string title);

        /// <summary>
        /// 查询详细信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CaseTpl getCaseByID(int id);
        CaseTpl getCaseByZyNo(string zyno);

        void AddCase(CaseTpl casetpl);

        void AddApply(Apply casetpl);

        void UpdateCase(CaseTpl casetpl);

        IPagedList<CaseTpl> GetCaseList(int page, int size);

    }
}
