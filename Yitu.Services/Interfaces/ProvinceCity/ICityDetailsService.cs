﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.ProvinceCity;

namespace zqdn.Services.Interfaces.ProvinceCity
{
    public interface ICityDetailsService : IBaseService<CityDetails>
    {
        IList<CityDetails> GetByParentID(int pid);

        //0：省份 1：城市 2：区县
        IList<CityDetails> GetByDepth(int depth);

        CityDetails GetByID(int ID);

        Hospital GetHospitalByID(int ID);

        List<Hospital> GetHospitalByCID(int CID);
    }
}
