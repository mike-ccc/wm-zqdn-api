﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace zqdn.Services.Interfaces
{

    public interface IBaseService<T> where T : class
    {
        void Create(T e);
        void Update(T e);
        void Delete(T e, bool definite = false);
        void Delete(Expression<Func<T, bool>> func, bool definite = false);
        T Get(params object[] keys);
        IEnumerable<T> GetAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> func);
    }
}
