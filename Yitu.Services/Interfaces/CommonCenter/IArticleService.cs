﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Article;

namespace zqdn.Services.Interfaces.CommonCenter
{
    public interface IArticleService
    {
        /// <summary>
        /// 获取名家谈手术
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        List<Article> getAllArticle(int? tid);

        /// <summary>
        /// 查询详细信息
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        Article getArticleByID(int aid);

        /// <summary>
        /// 管理后台获得所有手术资料
        /// </summary>
        /// <returns></returns>
        IPagedList<Article> getAllArticlePage(int page, int size);

        /// <summary>
        /// 新增文章
        /// </summary>
        /// <param name="obj"></param>
        void addArticle(Article obj);

        /// <summary>
        /// 修改观看次数
        /// </summary>
        /// <param name="obj"></param>
        void updateArticle(Article obj);

        /// <summary>
        /// 修改点赞，收藏次数
        /// </summary>
        /// <param name="obj"></param>
        void updateArticleStar(Article obj);
    }
}
