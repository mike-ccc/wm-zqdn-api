﻿using System;
using System.Collections.Generic;

namespace zqdn.Services
{
    public class BaseService<T> : Interfaces.IBaseService<T> where T : class
    {
        private readonly Repositories.Interfaces.IRepository<T> _repository;
        private readonly Repositories.Interfaces.IUnitOfWork _unitOfWork;

        public BaseService(Repositories.Interfaces.IRepository<T> repository,
            Repositories.Interfaces.IUnitOfWork unitOfWork)
        {
            this._repository = repository;
            this._unitOfWork = unitOfWork;
        }
        public void Create(T e)
        {
            _repository.Add(e);
            _unitOfWork.Commit();
        }

        public void Update(T e)
        {
            _repository.Update(e);
            _unitOfWork.Commit();
        }


        public void Delete(T e, bool definite = false)
        {
            _repository.Delete(e, definite);
            _unitOfWork.Commit();
        }

        public void Delete(System.Linq.Expressions.Expression<Func<T, bool>> func, bool definite = false)
        {
            _repository.Delete(func, definite);
            _unitOfWork.Commit();
        }


        public T Get(params object[] keys)
        {
            return _repository.GetByKeys(keys);
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public IEnumerable<T> Where(System.Linq.Expressions.Expression<Func<T, bool>> func)
        {
            return _repository.GetMany(func);
        }
    }
}
