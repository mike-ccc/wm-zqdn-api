﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.good;
using zqdn.Services.Interfaces.Ex;
using zqdn.Repositories;
using zqdn.Models.Ex;
using zqdn.Services.Interfaces.Case;

namespace zqdn.Services.Ex
{
    public class GoodService : BaseService<Goods>, IGoodService
    {
        private readonly IGoodRepository _goodrepository;
        private readonly IUnitOfWork _unitOfWork;

        public GoodService(IGoodRepository goodrepository, IUnitOfWork unitOfWork)
            : base(goodrepository, unitOfWork)
        {
            this._goodrepository = goodrepository;
            this._unitOfWork = unitOfWork;
        }

        public List<Goods> getAllGood()
        {
            return _goodrepository.GetAll().Where(o =>o.IsDeleted == false).ToList();
        }


        public void AddGood(Goods goodtpl)
        {
            _goodrepository.Add(goodtpl);
            _unitOfWork.Commit();
        }
        public IPagedList<Goods> GetGoodsList(int page, int size)
        {
            var query = _goodrepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }

        public Goods getGoodsByID(int id)
        {
            return _goodrepository.GetById(id);
        }
    }
}
