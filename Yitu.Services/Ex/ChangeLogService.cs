﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.good;
using zqdn.Services.Interfaces.Ex;
using zqdn.Repositories;
using zqdn.Models.Ex;
using zqdn.Services.Interfaces.Case;

namespace zqdn.Services.Ex
{
    public class ChangeLogService : BaseService<ChangeLog>, IChangeLogService
    {
        private readonly IChangeLogRepository _changelogrepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChangeLogService(IChangeLogRepository changelogrepository, IUnitOfWork unitOfWork)
            : base(changelogrepository, unitOfWork)
        {
            this._changelogrepository = changelogrepository;
            this._unitOfWork = unitOfWork;
        }

        public IPagedList<ChangeLog> getAllChangeLog(int page, int size)
        {
            var query = _changelogrepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true); 
        }

    }
}
