﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.ase;
using zqdn.Services.Interfaces.Case;
using zqdn.Repositories;
using zqdn.Models.Ex;
using zqdn.Repositories.Interfaces.Ex;
using zqdn.Services.Interfaces.Ex;

namespace zqdn.Services.Ex
{
    public class ExService : BaseService<ExTpl>, IExService
    {
        private readonly IExepository _exrepository;
        private readonly IChangeRepository _changerepository;
        private readonly IExLogRepository _exLogRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ExService(IExepository exrepository, IChangeRepository changerepository, IExLogRepository exLogRepository, IUnitOfWork unitOfWork)
            : base(exrepository, unitOfWork)
        {
            this._exrepository = exrepository;
            this._changerepository = changerepository;
            this._exLogRepository = exLogRepository;
            this._unitOfWork = unitOfWork;
        }

        public List<ExTpl> getAllEx()
        {
            return _exrepository.GetAll().Where(o => o.IsDeleted == false).ToList();
        }

        public ExTpl getExByID(int id)
        {
            return _exrepository.GetById(id);
        }

        public List<ExLogs> getLogsByType(int tid, int uid)
        {
            DateTime today = DateTime.Now;
            DateTime startDate = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0);
            DateTime endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            return _exLogRepository.GetAll().Where(o => o.UID == uid && o.Type == tid && o.IsDeleted == false && (o.CreatedTime >= startDate && o.CreatedTime <= endDate)).ToList();
        }

        public ExLogs getLogByType(int tid,int uid)
        {
            DateTime today = DateTime.Now;
            DateTime startDate = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0);
            DateTime endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            return _exLogRepository.GetAll().Where(o => o.UID == uid && o.Type == tid && o.IsDeleted == false && (o.CreatedTime >= startDate && o.CreatedTime <= endDate)).FirstOrDefault();
        }

        public ExLogs getLogByEid(int tid,int uid)
        {
            DateTime today = DateTime.Now;
            DateTime startDate = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0);
            DateTime endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            return _exLogRepository.GetAll().Where(o => o.UID == uid && o.Type == 4 && o.EID == tid && o.IsDeleted == false && (o.CreatedTime >= startDate && o.CreatedTime <= endDate)).FirstOrDefault();
        }

        
        public void AddEx(ExTpl extpl)
        {
            _exrepository.Add(extpl);
            _unitOfWork.Commit();
        }

        public void AddExLog(ExLogs exlogtpl)
        {
            _exLogRepository.Add(exlogtpl);
            _unitOfWork.Commit();
        }

        public void AddExChange(ChangeLog log)
        {
            _changerepository.Add(log);
            _unitOfWork.Commit();
        }
        public List<ChangeLog> GetChangeLogsByUID(int uid)
        {
            return _changerepository.GetAll().Where(o => o.IsDeleted == false && o.UID == uid).ToList();
        }

        public IPagedList<ExTpl> GetExList(int page, int size)
        {
            var query = _exrepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }
    }
}
