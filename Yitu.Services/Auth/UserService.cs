﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Auth;
using zqdn.Repositories.Interfaces;
using zqdn.Repositories.Interfaces.Auth;
using zqdn.Services.Interfaces;
using zqdn.Utils;
using zqdn.Repositories;

namespace zqdn.Services.Auth
{
    public class UserService : BaseService<User>, IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userroleRepository;
        private readonly IDoctorFilterRepository _doctorRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository,IUserRoleRepository userroleRepository,IDoctorFilterRepository doctorRepository, IUnitOfWork unitOfWork)
            : base(userRepository, unitOfWork)
        {
            this._userRepository = userRepository;
            this._userroleRepository = userroleRepository;
            this._doctorRepository = doctorRepository;
            this._unitOfWork = unitOfWork;
        }

        public User UserLogin(string username, string password,string openid,int type)
        {
            if (type == 1)
            {
                string encryptPwd = zqdn.Utils.Security.Encrypt.Md5Encrypt(password);
                return _userRepository.Get(o => ((o.UserName == username || o.Mobile == username)) && o.PassWord == encryptPwd);
            }
            else {
                return _userRepository.Get(o => (o.OpenId == openid ));
            }
        }

        public User GetUserByID(int id)
        {
            return _userRepository.GetById(id);
        }



        public User GetUserByMobile(string mobile)
        {
            return _userRepository.GetAll().Where(o =>o.Mobile==mobile && o.IsDeleted==false).FirstOrDefault();
        }

        public DoctorFilter getDoctorByMobile(string mobile) {
            return _doctorRepository.GetAll().Where(o => o.Mobile == mobile && o.IsDeleted == false).FirstOrDefault();
        }

        public DoctorFilter getDoctorByID(int id)
        {
            return _doctorRepository.GetById(id);
        }

        public User GetUserByOprnID(string openid)
        {
            return _userRepository.GetAll().Where(o => o.OpenId == openid && o.IsDeleted == false).FirstOrDefault();
        }


        public List<User> GetUserByHid()
        {
            return _userRepository.GetAll().Where(o => o.RoleID==2 && o.IsDeleted==false).ToList();
        }

        public List<User> GetUserByRoleID(int roleid)
        {
            return _userRepository.GetAll().Where(o => o.RoleID == roleid && o.IsDeleted == false).ToList();
        }

        public List<UserRole> GetUserRoles()
        {
            return _userroleRepository.GetAll().ToList();
        }

        public void AddUser(User user)
        {
            _userRepository.Add(user);
            _unitOfWork.Commit();
        }

        public void AddDoctorFilter(DoctorFilter doctorfilter)
        {
            _doctorRepository.Add(doctorfilter);
            _unitOfWork.Commit();
        }
        public void DeleteDoctorFilter(DoctorFilter doctorfilter)
        {
            _doctorRepository.Delete(doctorfilter);
            _unitOfWork.Commit();
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
            _unitOfWork.Commit();
        }

        public void UpdateDoctorFilter(DoctorFilter doctorfilter)
        {
            _doctorRepository.Update(doctorfilter);
            _unitOfWork.Commit();
        }


        ///管理员获得所有用户
        public IPagedList<User> GetUsers(int page, int size)
        {
            var query = _userRepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }

        public IPagedList<DoctorFilter> GetDoctorFilters(int page, int size)
        {
            var query = _doctorRepository.GetAll();
            return query.GetPage(e => e.ID, page, size, true);
        }
    }
}
