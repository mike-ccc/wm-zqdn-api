#yitu

## 前端目录结构：
```
/Content
    /js, javascript目录，建议使用Web Essentials进行minify
    /images, 图片资源
    /css, 此处的css文件不要进行编辑，直接编辑scss(sass);
    /uploads, 上传默认目录，可根据实际情况进行link，转移到其它磁盘；
    /libs, 前端所用到的类库，如：bootstrap, jquery, zepto.js等；
    /brackets, 用于后台的一套前端UI，相关示例可以在此处下载示例：http://bracket-demo.alinote.net/bracket.zip
```

## Mvc目录结果说明
```
Areas
    /Admin, 管理后台
Controllers
ViewModels, 视图模型映射，推荐使用，使用AutoMap与Model进行映射；
Views
```

## 项目结构说明

- Yitu.Models,
    * 数据模型，如User, Role, Doctor, Hospital等
    * AppEnums，Yitu用到的系统枚举
    * AppConsts, Yitu中用到的系统枚举值
    
- Yitu.Utils, 所有Utility类，如加密，AppConfig等；
- Yitu.Repositories，类似于DAL，数据访问层，使用EntityFramework, Repository和UnitOfWork实现；
- Yitu.Services，类似BLL层，面向业务，而不是面向数据；
- Yitu.Web，UI


## VS插件推荐
1. Web Essentials




