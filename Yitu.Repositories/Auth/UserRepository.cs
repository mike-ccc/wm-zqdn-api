﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Auth;
using zqdn.Repositories.Interfaces.Auth;

namespace zqdn.Repositories.Auth
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public class DoctorFilterRepository : RepositoryBase<DoctorFilter>, IDoctorFilterRepository
    {
        public DoctorFilterRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}




