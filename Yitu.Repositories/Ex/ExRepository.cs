﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Ex;
using zqdn.Repositories.Interfaces.Ex;
namespace zqdn.Repositories.Case
{
    public class ExRepository : RepositoryBase<ExTpl>, IExepository
    {
        public ExRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public class ChangeRepository : RepositoryBase<ChangeLog>, IChangeRepository
    {
        public ChangeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public class ExLogRepository : RepositoryBase<ExLogs>, IExLogRepository
    {
        public ExLogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
