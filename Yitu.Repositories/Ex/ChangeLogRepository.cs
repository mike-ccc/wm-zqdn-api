﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Case;
using zqdn.Models.Ex;
using zqdn.Repositories.Interfaces.good;
namespace zqdn.Repositories.Ex
{
    public class ChangeLogRepository : RepositoryBase<ChangeLog>, IChangeLogRepository
    {
        public ChangeLogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
