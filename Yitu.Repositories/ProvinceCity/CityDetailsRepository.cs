﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.ProvinceCity;
using zqdn.Repositories.Interfaces.ProvinceCity;

namespace zqdn.Repositories.ProvinceCity
{
    public class CityDetailsRepository : RepositoryBase<CityDetails>, ICityDetailsRepository
    {
        public CityDetailsRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
