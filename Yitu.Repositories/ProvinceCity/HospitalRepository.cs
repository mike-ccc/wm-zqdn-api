﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.ProvinceCity;
using zqdn.Repositories.Interfaces.ProvinceCity;

namespace zqdn.Repositories.ProvinceCity
{
    public class HospitalRepository : RepositoryBase<Hospital>, IHospitalRepository
    {
        public HospitalRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
