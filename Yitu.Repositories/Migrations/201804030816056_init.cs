namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        Type = c.Int(nullable: false),
                        Desc = c.String(maxLength: 200),
                        Content = c.String(nullable: false),
                        VideoUrl = c.String(maxLength: 50),
                        Pdfurl = c.String(),
                        ThumbImg = c.String(maxLength: 50),
                        Auth = c.String(maxLength: 10),
                        WatchCount = c.Int(nullable: false),
                        CreateUserID = c.Int(nullable: false),
                        DoctorID = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Articles");
        }
    }
}
