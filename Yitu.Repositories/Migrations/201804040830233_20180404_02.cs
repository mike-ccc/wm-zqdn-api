namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180404_02 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MetCity = c.String(maxLength: 20),
                        SpeakCity = c.String(maxLength: 20),
                        SpeakHos = c.String(maxLength: 50),
                        SpeakDep = c.String(maxLength: 50),
                        SpeakPost = c.String(maxLength: 50),
                        SpeakPhone = c.String(maxLength: 50),
                        SpeakContent = c.String(maxLength: 500),
                        SpeakTimePlan = c.String(maxLength: 500),
                        JoinCount = c.String(maxLength: 50),
                        SpeakRate = c.String(maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WriteTime = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        ZyNo = c.String(maxLength: 50),
                        CNo = c.String(nullable: false, maxLength: 50),
                        Name = c.String(maxLength: 50),
                        Sex = c.String(maxLength: 10),
                        Age = c.String(maxLength: 10),
                        Weight = c.String(maxLength: 10),
                        Med_YDQ = c.String(maxLength: 200),
                        Med_WCQ = c.String(maxLength: 200),
                        Med_SXQ = c.String(maxLength: 200),
                        MedCom_YDQ = c.String(maxLength: 200),
                        MedCom_WCQ = c.String(maxLength: 200),
                        MedCom_SXQ = c.String(maxLength: 200),
                        CaseImg = c.String(maxLength: 200),
                        ActiveHos = c.String(maxLength: 50),
                        ActiveID = c.Int(nullable: false),
                        MedCf_BTFN = c.String(maxLength: 200),
                        MedCf_YMTMD = c.String(maxLength: 200),
                        Med_PW_1 = c.String(maxLength: 200),
                        Med_PW_2 = c.String(maxLength: 200),
                        Med_PW_3 = c.String(maxLength: 200),
                        Med_PW_4 = c.String(maxLength: 200),
                        Med_GZ = c.String(maxLength: 500),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Exes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 200),
                        Option = c.String(maxLength: 500),
                        TureOption = c.String(maxLength: 200),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Goods",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        RequireJF = c.String(maxLength: 20),
                        GoodImg = c.String(maxLength: 200),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Users", "Integral", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Mobile", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Mobile", c => c.String(nullable: false));
            DropColumn("dbo.Users", "Integral");
            DropTable("dbo.Goods");
            DropTable("dbo.Exes");
            DropTable("dbo.Cases");
            DropTable("dbo.Applies");
        }
    }
}
