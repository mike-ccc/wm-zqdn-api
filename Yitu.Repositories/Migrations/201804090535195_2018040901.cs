namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2018040901 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Area", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Area");
        }
    }
}
