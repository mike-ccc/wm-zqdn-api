namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180429_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExLogs", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExLogs", "Type");
        }
    }
}
