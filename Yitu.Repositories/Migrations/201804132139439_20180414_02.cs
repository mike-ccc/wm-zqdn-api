namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180414_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaseTpls", "DepNo", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CaseTpls", "DepNo");
        }
    }
}
