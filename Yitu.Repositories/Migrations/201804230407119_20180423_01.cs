namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180423_01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExLogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UID = c.Int(nullable: false),
                        EID = c.Int(nullable: false),
                        Answer = c.String(),
                        isTrue = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ExLogs");
        }
    }
}
