namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180414_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Applies", "CreateUserID", c => c.Int(nullable: false));
            AddColumn("dbo.ChangeLogs", "Socure", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChangeLogs", "Socure");
            DropColumn("dbo.Applies", "CreateUserID");
        }
    }
}
