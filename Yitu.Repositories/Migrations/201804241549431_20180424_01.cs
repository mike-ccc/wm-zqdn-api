namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180424_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaseTpls", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CaseTpls", "Status");
        }
    }
}
