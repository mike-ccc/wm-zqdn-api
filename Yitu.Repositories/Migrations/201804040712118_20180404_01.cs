namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180404_01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 32),
                        PassWord = c.String(nullable: false, maxLength: 32),
                        Name = c.String(nullable: false, maxLength: 32),
                        Province = c.Int(nullable: false),
                        City = c.Int(nullable: false),
                        Hospital = c.Int(nullable: false),
                        Deparment = c.String(),
                        DoctorOffice = c.String(),
                        Mobile = c.String(nullable: false),
                        DoctorCertificate = c.String(),
                        RoleID = c.Int(nullable: false),
                        Domain = c.String(),
                        Status = c.Int(nullable: false),
                        OpenID = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        Name = c.String(),
                        Desc = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Articles", "StarCount", c => c.Int(nullable: false));
            AddColumn("dbo.Articles", "LikeCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "LikeCount");
            DropColumn("dbo.Articles", "StarCount");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Users");
        }
    }
}
