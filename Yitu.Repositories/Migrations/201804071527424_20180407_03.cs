namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180407_03 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChangeLogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GID = c.Int(nullable: false),
                        UID = c.Int(nullable: false),
                        Address = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        ModifiedTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ChangeLogs");
        }
    }
}
