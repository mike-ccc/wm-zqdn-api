namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180407_01 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Exes", newName: "ExTpls");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ExTpls", newName: "Exes");
        }
    }
}
