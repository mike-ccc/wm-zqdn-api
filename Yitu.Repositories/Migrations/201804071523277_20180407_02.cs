namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180407_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Goods", "Stock", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Goods", "Stock");
        }
    }
}
