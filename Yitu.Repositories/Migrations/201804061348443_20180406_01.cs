namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180406_01 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Cases", newName: "CaseTpls");
            AddColumn("dbo.CaseTpls", "CreateUserID", c => c.Int(nullable: false));
            AlterColumn("dbo.CaseTpls", "ActiveID", c => c.String(maxLength: 50));
            AlterColumn("dbo.Users", "Province", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "City", c => c.String());
            AlterColumn("dbo.Users", "Hospital", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Hospital", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "City", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Province", c => c.Int(nullable: false));
            AlterColumn("dbo.CaseTpls", "ActiveID", c => c.Int(nullable: false));
            DropColumn("dbo.CaseTpls", "CreateUserID");
            RenameTable(name: "dbo.CaseTpls", newName: "Cases");
        }
    }
}
