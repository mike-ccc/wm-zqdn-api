namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180408_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "NikeName", c => c.String());
            AddColumn("dbo.Users", "HeadImg", c => c.String());
            AlterColumn("dbo.Users", "OpenID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "OpenID", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "HeadImg");
            DropColumn("dbo.Users", "NikeName");
        }
    }
}
