namespace zqdn.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20180422_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChangeLogs", "Name", c => c.String());
            AddColumn("dbo.ChangeLogs", "Mobile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChangeLogs", "Mobile");
            DropColumn("dbo.ChangeLogs", "Name");
        }
    }
}
