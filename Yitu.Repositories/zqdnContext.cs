﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zqdn.Repositories
{
    public sealed class zqdnContext : DbContext
    {
        public zqdnContext()
            : base("DB_Zqdn_Dev")
        {
        }
        public zqdnContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public void Commit()
        {
            base.SaveChanges();
        }


        #region Article
        public DbSet<zqdn.Models.Article.Article> Article { get; set; }
        #endregion

        #region User
        public DbSet<zqdn.Models.Auth.User> User { get; set; }
        #endregion

        #region UserRole
        public DbSet<zqdn.Models.Auth.UserRole> UserRole { get; set; }
        #endregion

        #region Case
        public DbSet<zqdn.Models.Case.CaseTpl> Case { get; set; }
        #endregion

        #region Apply
        public DbSet<zqdn.Models.Apply.Apply> Apply { get; set; }
        #endregion

        #region Ex
        public DbSet<zqdn.Models.Ex.ExTpl> Ex { get; set; }
        #endregion

        #region Good
        public DbSet<zqdn.Models.Ex.Goods> Good { get; set; }
        #endregion

        #region ChangeLogs
        public DbSet<zqdn.Models.Ex.ChangeLog> ChangeLog { get; set; }
        #endregion

        #region DoctorFilter
        public DbSet<zqdn.Models.Auth.DoctorFilter> DoctorFilter { get; set; }
        #endregion

        #region ExLogs
        public DbSet<zqdn.Models.Ex.ExLogs> ExLogs { get; set; }
        #endregion

    }
}
