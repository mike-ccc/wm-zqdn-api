﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Case;
using zqdn.Models.Apply;
using zqdn.Repositories.Interfaces.ase;

namespace zqdn.Repositories.Case
{
    public class CaseRepository : RepositoryBase<CaseTpl>, ICaseRepository
    {
        public CaseRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public class ApplyRepository : RepositoryBase<Apply>, IApplyRepository
    {
        public ApplyRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
