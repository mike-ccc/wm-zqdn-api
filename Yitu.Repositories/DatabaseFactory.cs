﻿using System;

namespace zqdn.Repositories
{
    public interface IDatabaseFactory : IDisposable
    {
        zqdnContext GetDbContext();
    }

    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private readonly zqdnContext _dbContext;

        public DatabaseFactory(string nameOrConnectionString)
        {
            this._dbContext = new zqdnContext(nameOrConnectionString);
        }
        public DatabaseFactory(zqdnContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public zqdnContext GetDbContext()
        {
            return this._dbContext;
        }
             
        protected override void DisposeCore()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }
    }
}