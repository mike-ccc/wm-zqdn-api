﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Auth;

namespace zqdn.Repositories.Interfaces.Auth
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
    }
}
