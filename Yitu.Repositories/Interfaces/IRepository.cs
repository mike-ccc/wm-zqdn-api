﻿using System;
using System.Linq;
using System.Linq.Expressions;
using PagedList;

namespace zqdn.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity, bool definite = false);
        void Delete(Expression<Func<T, bool>> where, bool definite = false);
        T GetById(long id);
        T GetById(string id);
        T Get(Expression<Func<T, bool>> where);
        T GetByKeys(params object[] keys);
        bool Any(Expression<Func<T, bool>> where);
        IQueryable<T> GetAll();
        IQueryable<T> GetMany(Expression<Func<T, bool>> where);
        IPagedList<T> GetPage<TOrder>(Page page, Expression<Func<T, bool>> where, Expression<Func<T, TOrder>> order, bool isOrderDesc = false);
        IPagedList<T> GetPage<TOrder>(Expression<Func<T, bool>> where, Expression<Func<T, TOrder>> order, int page, int pageSize, bool isOrderDesc = false);

        void ExecuteSqlCommand(string sql, params object[] parameters);
    }
}
