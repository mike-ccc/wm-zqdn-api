﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Case;
using zqdn.Models.Apply;

namespace zqdn.Repositories.Interfaces.ase
{
    public interface ICaseRepository : IRepository<CaseTpl>
    {

    }
    public interface IApplyRepository : IRepository<Apply>
    {

    }
}
