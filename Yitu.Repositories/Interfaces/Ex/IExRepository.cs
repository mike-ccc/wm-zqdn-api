﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Ex;

namespace zqdn.Repositories.Interfaces.Ex
{
    public interface IExepository : IRepository<ExTpl>
    {

    }

    public interface IChangeRepository : IRepository<ChangeLog>
    {

    }

    public interface IExLogRepository : IRepository<ExLogs>
    {

    }
}
