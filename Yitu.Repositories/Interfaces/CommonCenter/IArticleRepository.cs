﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.Article;

namespace zqdn.Repositories.Interfaces.CommonCenter
{
    public interface IArticleRepository : IRepository<Article>
    {
    }
}
