﻿namespace zqdn.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
