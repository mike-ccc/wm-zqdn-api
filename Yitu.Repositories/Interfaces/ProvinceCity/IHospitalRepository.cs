﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zqdn.Models.ProvinceCity;

namespace zqdn.Repositories.Interfaces.ProvinceCity
{
    public interface IHospitalRepository : IRepository<Hospital> 
    {

    }
}
