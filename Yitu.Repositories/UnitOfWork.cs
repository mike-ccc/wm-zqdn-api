﻿namespace zqdn.Repositories
{
    public class UnitOfWork : Interfaces.IUnitOfWork
    {
        private readonly IDatabaseFactory _databaseFactory;
        private zqdnContext _dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this._databaseFactory = databaseFactory;
        }

        protected zqdnContext DataContext
        {
            get { return _dataContext ?? (_dataContext = _databaseFactory.GetDbContext()); }
        }

        public void Commit()
        {
            DataContext.Commit();
        }
    }
}
