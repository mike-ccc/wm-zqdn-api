﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace zqdn.Utils.Security
{
    public static class Encrypt
    {
        public static string Md5Encrypt(string cleanText)
        {
            byte[] result = Encoding.Default.GetBytes(cleanText);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);
            return BitConverter.ToString(output).Replace("-", "");
        }

        public static string DesEncrypt(string cleanText, string key, string iv)
        {
            byte[] btKey = Encoding.UTF8.GetBytes(Md5Encrypt(key).Substring(0, 8));
            byte[] btIV = Encoding.UTF8.GetBytes(Md5Encrypt(iv).Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            using (MemoryStream ms = new MemoryStream())
            {
                byte[] inData = Encoding.UTF8.GetBytes(cleanText);
                using (CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(btKey, btIV), CryptoStreamMode.Write))
                {
                    cs.Write(inData, 0, inData.Length);

                    cs.FlushFinalBlock();
                }
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static string DESDecrypt(string encryptedString, string key, string iv)
        {
            byte[] btKey = Encoding.UTF8.GetBytes(Md5Encrypt(key).Substring(0, 8));
            byte[] btIV = Encoding.UTF8.GetBytes(Md5Encrypt(iv).Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            using (MemoryStream ms = new MemoryStream())
            {
                byte[] inData = Convert.FromBase64String(encryptedString);
                using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(btKey, btIV), CryptoStreamMode.Write))
                {
                    cs.Write(inData, 0, inData.Length);
                    cs.FlushFinalBlock();
                }
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }
    }
}
